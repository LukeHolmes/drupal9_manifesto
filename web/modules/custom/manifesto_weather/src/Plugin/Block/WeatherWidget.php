<?php

namespace Drupal\manifesto_weather\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Weather Display' Block.
 *
 * @Block(
 *   id = "weather_widget",
 *   admin_label = @Translation("Weather Widget"),
 *   category = @Translation("Weather Widget"),
 * )
 */
class WeatherWidget extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->getCurrentNodePostcode(),
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }


  /**
   * Function to retrieve the postcode from the current entity...
   * @return string
   */
  public function getCurrentNodePostcode() {

    $node = \Drupal::routeMatch()->getParameter('node');

    if($node->getType() === 'article') {
      $postcode = $node->get('field_postcode')->value;
    }

    if($postcode) {
      return $this->getWeather($postcode);
    }
    else {
      return 'No Postcode associated with this entity';
    }

  }


  public function getWeather($the_postcode) {

    // Hard coded, just for demo purposes. This would NOT be the best approach in a non-test scenario...
    $url = 'https://drupal9-manifesto.ddev.site/api/rest-endpoint/weather';

    // Note that we'd usually be passing through the given postcode as a paramter. In this example, I didn't
    // deem it necessary...

    //  Initiate curl
    $ch = curl_init();
    // Will return the response, if false it print the response
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Set the url
    curl_setopt($ch, CURLOPT_URL,$url);
    // Execute
    $result=curl_exec($ch);
    // Closing
    curl_close($ch);

    // Will dump a beauty json :3
    $decoded = json_decode($result, true);

      return $decoded["data"][0]["weather"];
    }

}
