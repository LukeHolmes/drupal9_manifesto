<?php

namespace Drupal\manifesto_weather\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Implementing our example JSON api.
 */
class CustomRestController {

  /**
   * Callback for the API.
   */
  public function renderApi() {

    return new JsonResponse([
      'data' => $this->getWeatherData(),
      'method' => 'GET',
    ]);
  }

  /**
   * Return a random weather value for this example...
   */
  public function getWeatherData() {

    // Generate a random value for the weather...
    $weather_arr = array('sunny', 'cloudy', 'rainy');
    $random_weather_type_key = array_rand($weather_arr, 1);
    $random_weather_value = $weather_arr[$random_weather_type_key];

    return [
      [
        "postcode" => "E1 5JL",
        "weather" => $random_weather_value,
      ],

    ];
  }

}
